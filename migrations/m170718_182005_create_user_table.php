<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170718_182005_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'username' => $this->string(),
            'password' => $this->string(),
            'authkey' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
