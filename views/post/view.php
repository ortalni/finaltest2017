<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Post;
use app\models\Category;
use app\models\Status;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<p>
	
	<?php if (\Yii::$app->user->can('updatePost', ['user' =>$model]) ){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     <?php } ?>
        <!--?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?-->
	<?php if (\Yii::$app->user->can('deletePost', ['user' =>$model]) ){ ?>
	
	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		 <?php } ?>
    </p>
    
	<!--[
				'attribute' => 'statusname',
				'value' => 'findStatus.name',
				'filter' => [  1 => 'Published', 2 => 'Wating', 3 => 'Rejected',]
		],
	-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            //'category',
			
			[ // the category name 
				'label' => $model->attributeLabels()['category'],
				'value' => $model->findCategory->category_name,	
			],	
			
            'author',
			/*[ // the Author name 
				'label' => $model->attributeLabels()['user'],
				'value' => $model->findAuthor->username,	
			],*/
			
            //'status',
			
			[ // the status name 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->findStatus->status_name,	
			],	
			
			
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
